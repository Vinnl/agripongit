import { useEffect } from "react";
import { useInView as useInViewOriginal } from "react-intersection-observer";

type Options = Parameters<typeof useInViewOriginal>[0] & {
  onInView?: () => void;
};
export const useInView = (options?: Options): ReturnType<typeof useInViewOriginal> => {
  const { onInView, ...otherOptions } = options ?? {};
  const returnValue = useInViewOriginal(otherOptions);

  useEffect(() => {
    if (returnValue.inView && typeof options?.onInView === "function") {
      options.onInView();
    }
  }, [returnValue.inView]);

  return returnValue;
}