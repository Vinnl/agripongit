import Head from "next/head";
import React, { useEffect, useState } from "react";
import { SiMastodon, SiTwitter } from "react-icons/si";
import { MdHome } from "react-icons/md";
import styles from "./index.module.css";
import { A, Section } from "../components/html";
import { Repository } from "../components/Repository";
import { useInView } from "../hooks/inView";
import { tutorial } from "../tutorial";

export default function Home() {
  const [currentSection, setCurrentSection] = useState(-1);
  const { ref: topRef } = useInView({ onInView: () => setCurrentSection(-1)});
  const [inPresentationMode, setInPresentationMode] = useState(false);

  useEffect(() => {
    if (typeof document !== "undefined" && document.location.search === "?presentation") {
      setInPresentationMode(true);
    }
  }, []);

  const sections = tutorial.map((section, i) => {
    return (
      <Section key={"section" + i} onInView={() => setCurrentSection(i)}>
        {section.text}
      </Section>
    );
  });
  const commands = tutorial.slice(0, currentSection + 1).map(section => section.command);

  return (
    <div className={inPresentationMode ? styles.presentation : ""}>
      <Head>
        <title>A Grip On Git — A simple, visual Git tutorial</title>
        <link rel="icon" href="/favicon.ico" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@VincentTunru" />
        <meta property="twitter:title" content="A Grip On Git — A simple, visual Git tutorial" />
        <meta property="twitter:description" content="Level up from just being able to *use* Git to *understanding* it, and get a better grip on one of the most important tools in software engineering." />
        <meta property="twitter:image" content="https://agripongit.VincentTunru.com/logo.svg" />
        <meta property="og:title" content="A Grip On Git — A simple, visual Git tutorial" />
        <meta property="og:description" content="Level up from just being able to *use* Git to *understanding* it, and get a better grip on one of the most important tools in software engineering." />
        <meta property="og:type" content="article" />
        <meta property="og:article:tag" content="git" />
        <meta property="og:article:tag" content="version control" />
        <meta property="og:locale" content="en-GB" />
        <meta property="og:url" content="https://agripongit.VincentTunru.com" />
        <meta property="og:image" content="https://agripongit.VincentTunru.com/logo.svg" />
      </Head>

      <main className="bg-gray-800 text-white min-h-screen flex">
        <div className={`${styles.content} lg:w-1/2 lg:pl-36 px-10 z-10`}>
          <header ref={topRef} className="text-center py-44 space-y-10">
            <img src="/logo.svg" alt="" className="inline-block"/>
            <h1 className="text-5xl text-blue-400">A Grip On Git</h1>
            <p className="text-3xl">A Simple, Visual Git Tutorial</p>
            <p className="flex flex-row items-center justify-center">
              <span>Reading time ~11 minutes · By <A href="https://VincentTunru.com">Vincent&nbsp;Tunru</A></span> <TwitterLink/> <MastodonLink/>
            </p>
          </header>
          <section className="space-y-5">
            <p>Have you <A href="https://xkcd.com/1597/" title="xkcd on Git">memorised a few Git commands</A>, without actually understanding what's going on? Then you've come to the right place! This how-to will help you level up in Git, going from being able to <em>use</em> Git to properly <em>understanding</em> it, getting a better grip on what is arguably one of the most important tools in software development at the moment.</p>
            <p>This interactive tutorial visualises what is happening when you're using Git. As you scroll down the page, you will be guided through the most important basic concepts of Git, while applying them to a visualised example repository.</p>
          </section>
          {sections}
          <section className="space-y-5 py-1/2 bg-gray-800">
            <p>That wraps up this Git tutorial! I hope it helped you grasp the fundamental concepts, so that you won't feel completely lost when you have to perform more advanced operations in Git. If you're interested, do check out <A href="https://gitlab.com/VincentTunru/agripongit" title="A Grip on Git repository">the source code</A> of this tutorial (naturally, in a Git repository), and let me know what you think.</p>
            <p>
              Thanks,<br/>
              <span className="flex items-center py-1"><A href="https://VincentTunru.com">Vincent</A> <TwitterLink/> <MastodonLink/></span>
            </p>
            <p className={`${styles.closer} hidden text-9xl font-serif py-10`}>
              Fin.
            </p>
            <p className={`${styles.credits} hidden text-2xl flex-col gap-5`}>
              <WebsiteLink/> <TwitterLink wordmark={true}/> <MastodonLink wordmark={true}/>
            </p>
          </section>
        </div>
        <div className={`${styles.visualisation} w-full lg:w-1/2 fixed right-0 h-full z-0`}>
          <Repository commands={commands}/>
        </div>
      </main>
    </div>
  );
}

const WebsiteLink: React.FC = () => {
  return <>
    <a href="https://VincentTunru.com" className="inline-flex items-center gap-1 px-2 text-gray-100 hover:text-blue-400 focus:text-blue-400">
      <MdHome aria-hidden={true} />
      <span>Vincent Tunru.com</span>
    </a>
  </>;
};

const TwitterLink: React.FC<{ wordmark?: boolean }> = (props = { wordmark: false }) => {
  return <>
    <a href="https://twitter.com/VincentTunru" className="inline-flex items-center gap-1 px-2 text-gray-100 hover:text-blue-400 focus:text-blue-400">
      <SiTwitter aria-hidden={true} />
      <span aria-label="@VincentTunru on Twitter" className={props.wordmark ? "" : "sr-only"}>@VincentTunru</span>
    </a>
  </>;
};

const MastodonLink: React.FC<{ wordmark?: boolean }> = (props = { wordmark: false }) => {
  return <>
    <a href="https://fosstodon.org/@VincentTunru" className="inline-flex items-center gap-1 px-2 text-gray-100 hover:text-blue-400 focus:text-blue-400">
      <SiMastodon aria-hidden={true}/>
      <span aria-label="Vincent on Mastodon" className={props.wordmark ? "" : "sr-only"}>@VincentTunru@fosstodon.org</span>
    </a>
  </>;
};
