import React, { FC, HTMLProps } from "react";
import { MdChevronRight } from "react-icons/md";
import { useInView } from "../hooks/inView";

type AProps = HTMLProps<HTMLAnchorElement>;
export const A: FC<AProps> = (props) => {
  const className = `text-blue-400 hover:underline hover:bg-blue-400 hover:text-black ${props.className}`;
  return (
    <a {...props} className={className}>
      {props.children}
    </a>
  );
};

type SectionProps = HTMLProps<HTMLElement> & {
  onInView?: () => void;
};
export const Section: FC<SectionProps> = (props) => {
  const { ref } = useInView({ onInView: props.onInView, threshold: 0.5 })
  const className = `pb-20 py-1/2 ${props.className}`;

  return (
    <section {...props} ref={ref} className={className}>
      <div className="space-y-5 bg-blueGray-700 p-5 rounded opacity-95">
        {props.children}
      </div>
    </section>
  );
};

type CommandProps = unknown;
export const Command: FC<CommandProps> = (props) => {
  return (
    <code className="CommandPrompt block pl-5">
      <MdChevronRight className="inline-block text-blueGray-400 text-xl mr-2" aria-hidden="true"/>
      <span>{props.children}</span>
    </code>
  );
};

type CodeProps = {};
export const Code: FC<CodeProps> = (props) => {
  return (
    <code className="font-bold bg-gray-600 rounded px-1">
      {props.children}
    </code>
  );
};
